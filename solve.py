from datetime import datetime


def read_lines(file_name):
    with open(file_name, "rb") as f:
        lines = f.read().splitlines()
    return lines


def grouped_by_lengths(lines):
    len_lines = {}
    for line in lines:
        length = len(line)
        if length in len_lines:
            len_lines[length] += [line]
        else:
            len_lines[length] = [line]

    min_len = min([len_line[0] for len_line in len_lines.items()])

    return len_lines, min_len


def print_len_lines(len_lines):
    for len_line in len_lines.items():
        print(len_line[0], len_line[1][:10])


def check_substr(s, len_lines):
    return len(s) in len_lines and s in len_lines[len(s)]


def check(s, len_lines, check_len, min_len, is_initial=True):
    if not is_initial and len(s) == 0:
        return True

    if is_initial:
        max_len = len(s) - 1
    else:
        max_len = len(s)

    while check_len <= max_len:
        if check_substr(s[:check_len], len_lines) and check(s[check_len:], len_lines, min_len, min_len, False):
            return True
        else:
            check_len += 1

    return False


def find_biggest(len_lines, min_len):
    for lines in len_lines.values():
        for line in lines:
            if check(line, len_lines, min_len, min_len):
                return line
    return None


def print_answer(file_name):
    start_time = datetime.now()

    lines = read_lines(file_name)
    lines.sort(key=lambda line: len(line), reverse=True)

    len_lines, min_len = grouped_by_lengths(lines)
    biggest = find_biggest(len_lines, min_len)

    time_elapsed = datetime.now() - start_time

    if biggest:
        print(biggest.decode("utf-8"))
    else:
        print("Not found")

    print("Finished in "+str(time_elapsed.microseconds / 1000)+" ms")


print_answer("words.txt")
