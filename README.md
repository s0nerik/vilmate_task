# Overview

### Algorithm

1. Read the entire input file in a binary mode (this adds additional performance boost)
2. Sort each line in descending order using line's length as a key
3. Group lines by their lengths (Python's map preserves insertion order, so items in a resulting map would also be sorted by length in a descending order)
4. Iterate through each `line` of input file and invoke a `check` function, which will do the following:
   1. Check whether it is a recursive call. If it is and `s` (currently checked substring) is empty - return True.
   2. Initialize `max_len` with `len(s)` or `len(s) - 1` if it's not a recursive call
   3. While `check_len` is less than or equal to  `max_len` do the following:
      1. Check whether `s[:check_len]` is included in file and, if so, do the recursive call to a `check` function with `s[check_len:]` as `s`.
5. Return `line` if `check` has returned `True`.
6. Print results

### Author's note

While there are more possible optimizations that can be incorporated in the algorithm that can reduce the execution time dramatically (like switching to a memory views instead of copying substrings each time while doing `line[start,end]`), they will certainly make code less readable and harder to understand correctly. The execution time of ~150 ms (Windows 10, Python 3.6, Intel Core i7 7700HQ, 16GB DDR4, SSD) on a `words.txt` file (over 170k lines) to me seems pretty reasonable and it will not get too nasty even given a file with over 1m lines considering all the optimizations done.